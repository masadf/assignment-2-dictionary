section .text 

global exit
global print_error
global print_string
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60			
    syscall

print_error:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 2
    mov rax, 1
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi	
    mov rdx, rax	
    mov rdi, 1	
    mov rax, 1	
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_char:
    push rdi
    mov rax, 1			
    mov rdx, 1						
    mov rdi, 1			
    mov rsi, rsp			
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r9, rsp
    mov r10, 10
    push qword 0
    .loop:
        xor rdx, rdx
        div r10
        add rdx, 0x30
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jnz .loop
    mov rdi, rsp
    push r9
    call print_string
    pop rsp
    ret 

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .print_val  
    push rdi
    mov rdi, '-'
    call print_char	
    pop rdi					
    neg rdi	
    .print_val:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        xor rax, rax                    
        mov al, byte[rdi]             
        cmp al, byte[rsi]            
        jne .noteq                   
        cmp al, 0                    
        je .eq                       
        inc rdi                
        inc rsi               
        jmp .loop                 
    .noteq:
        xor rax, rax                    
        ret
    .eq:
        mov rax, 1                   
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push qword 0				
    xor rax, rax			
    mov rdx, 1			
    xor rdi, rdi		
    mov rsi, rsp	
    syscall
    pop rax			
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi ; адрес буфера
    mov r13, rsi ; размер буфера
    dec r13 ; тк нуль-терминант
    xor r14, r14 ; счётчик символов
    .trim_start:
        call read_char	
        cmp rax, 0x0	
        je .exit
        cmp rax, 0x20	
        je .trim_start	
        cmp rax, 0x9	
        je .trim_start	
        cmp rax, 0xA	
        je .trim_start	
    .loop:
        cmp rax, 0x0	
        je .exit	   
        cmp rax, 0x20	
        je .exit	
        cmp rax, 0x9	
        je .exit	
        cmp rax, 0xA	
        je .exit	
        mov [r12+r14], al
        inc r14
        cmp r14, r13
        jg .error   ; если больше размера буфера
        call read_char
        jmp .loop
    .error: 
        pop r14
        pop r13
        pop r12
        mov rax, 0
        ret
    .exit:
        mov byte[r12+r14], 0			
	mov rax, r12		
	mov rdx, r14	
        pop r14
        pop r13
        pop r12	
        ret

read_string:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi 
    dec r13 
    xor r14, r14 
    .trim_start:
        call read_char
        cmp rax, 0x0
        je .exit
	cmp rax, 0x9
        je .trim_start
        cmp rax, 0xA
        je .trim_start
    .loop:
        cmp rax, 0x0
        je .exit
	cmp rax, 0x9
        je .exit        
	cmp rax, 0xA
        je .exit
        mov [r12+r14], al
        inc r14
        cmp r14, r13
        jg .error  
        call read_char
        jmp .loop
    .error:
        pop r14
        pop r13
        pop r12
        mov rax, 0
        ret
    .exit:
        mov byte[r12+r14], 0    
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax	
    xor r9, r9	; счётчик
    mov r10, 10 ; множитель
    .loop:
        xor r8, r8		
        mov r8b, byte[rdi + r9]	
        cmp r8b, 0x30	
        jb .exit	
        cmp r8b, 0x39
        ja .exit	
        sub r8b, 0x30
        mul r10		
        add rax, r8	
        inc r9		
        jmp .loop	
    .exit:
        mov rdx, r9	
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'		
    je .for_neg		
    jmp parse_uint
    .for_neg:
        inc rdi			
	call parse_uint
	cmp rdx, 0
	je  .end
	inc rdx
        neg rax
    .end:			
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    call string_length
    inc rax
    pop rdi
    cmp rax, rdx
    jg .error
    .loop:
        xor r8, r8
        mov r8b, byte[rdi]
        mov byte[rsi], r8b
        inc rdi
        inc rsi
        cmp r8b, 0
        jne .loop
        ret
    .error:
        xor rax, rax
        ret

