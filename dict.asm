%define cell_size 8

extern string_equals

section .text

global find_word

find_word:
    .loop:
        test rsi, rsi
        jz .nok
        push rsi
	add rsi, cell_size
        push rdi
        call string_equals
        pop rdi
        pop rsi
        cmp rax, 1
        jz .ok
        mov rsi, [rsi]
        jmp .loop
    .ok:
        mov rax, rsi
        ret
    .nok:
        xor rax, rax
        ret
