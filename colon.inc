%define next_element 0

%macro colon 2
     %ifid %2
        %2: dq next_element
        %define next_element %2
     %else
        %error "wrong ID"
     %endif

     %ifstr %1
        db %1, 0
     %else
        %error "key should be string"
     %endif
%endmacro
