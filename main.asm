%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 255
%define DATA_SHIFT 8
%define ERROR_CODE 1

global _start

extern find_word

section .bss
buffer: times BUFFER_SIZE resb 0

section .data

buffer_overflow_msg: db "Buffer overflow", 0
key_error_msg: db "Key is not found", 0

section .text

_start:
    mov rdi, buffer               
    mov rsi, BUFFER_SIZE          
    call read_string                
    test rax, rax                 
    jz .buffer_overflow_error    

    mov rdi, rax                  
    mov rsi, next_element         
    push rdx                      
    call find_word                
    pop rdx                       
    test rax, rax                 
    je .key_error
    
    mov rdi, rax                  
    add rdi, DATA_SHIFT                    
    inc rdi                    
    add rdi, rdx                  
    call print_string
    jmp .end

    .buffer_overflow_error:
        mov rdi, buffer_overflow_msg
        call print_error
	mov rdi, ERROR_CODE
        jmp .end

    .key_error:
        mov rdi, key_error_msg
        call print_error
	mov rdi, ERROR_CODE

    .end:
      call print_newline
      xor rdi rdi
      call exit
